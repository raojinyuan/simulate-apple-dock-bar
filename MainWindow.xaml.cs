﻿using System;
using System.Windows;
using System.Drawing;
using IWshRuntimeLibrary;
using System.Windows.Media;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using System.Windows.Media.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Management;
using System.Collections;
using static System.Net.WebRequestMethods;
using System.Windows.Documents;
using System.ComponentModel;
using Newtonsoft.Json;
using System.Security.Policy;
using Microsoft.Win32;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Windows.Shapes;

namespace WpfApp1
{
    public partial class MainWindow : Window
    {
        public static string folderToSave = System.AppDomain.CurrentDomain.BaseDirectory.ToString();

        public List<fileInfo> fileInfos = new List<fileInfo>();

        public static List<RootMydata> rootobject;

        public MainWindow()
        {
            InitializeComponent();
            # region 搜索电脑应用
            ArrayList disks = new ArrayList();
            SelectQuery selectQuery = new SelectQuery("select * from win32_logicaldisk");
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(selectQuery);

            foreach (ManagementObject disk in searcher.Get())
            {
                if (disk["Name"].ToString() == "c:" || disk["Name"].ToString() == "C:") continue;

                Common.AddPathInfo(disk["Name"].ToString() + "\\", out fileInfos);
            }

            string start = "c:\\ProgramData\\";
            Common.AddPathInfo(start, out fileInfos);
            string jsondata = JsonConvert.SerializeObject(fileInfos);

            using (StreamWriter sw = new StreamWriter(folderToSave + @"\\data.json"))  //将string 写入json文件
            {
                sw.WriteLine(jsondata);
            }
            #endregion
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = 1380;
            double windowHeight = this.Height;

            this.Left = (screenWidth - windowWidth)/2;
            this.Top = 880;

            var jsonStr = Common.GetJsonFile(folderToSave + @"\\data.json");
            rootobject = JsonConvert.DeserializeObject<List<RootMydata>>(jsonStr);
            if (Equals(rootobject))
            {
                return;
            }
            int itemsum = 0;
            foreach (var itme in rootobject)
            {
                if (!System.IO.File.Exists(itme.ExefilePath))
                {
                    continue;
                }
                if (!System.IO.File.Exists(itme.fileImg))
                {
                    continue;
                }
                itemsum +=1;
                if (itemsum > 12)
                {
                    continue;
                }
                this.rootobjectList.Items.Add(itme);
            }
        }

        public void OpenExe_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            string url = button.Content.ToString();
            if (url.Length <= 0)
            {
                return;
            }
            Common.OpenSDRSharp(url);
        }

    }

    public class RootMydata
    {
        public string fileImg { get; set; }
        public string ExefilePath { get; set; }
        public string fileName { get; set; }
    }


}
